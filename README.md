# Surf Shop

- Set up
```HTML
git clone https://github.com/rpb99/surf-shop.git

```
- install nodemon
```HTML
npm i -g nodemon
cd surf-shop
```
- run nodemon
```HTML
nodemon
```
-open browser
```HTML
localhost:3000
```