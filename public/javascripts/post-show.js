
mapboxgl.accessToken = mapBoxToken;

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v10',
    center: post.geometry.coordinates,
    zoom: 5
});



// create a HTML element for each feature
var el = document.createElement('div');
el.className = 'marker';

// make a marker for each feature and add to the map
new mapboxgl.Marker(el)
    .setLngLat(post.geometry.coordinates)
    .setPopup(new mapboxgl.Popup({ offsset: 25 }) // add popups
    .setHTML('<h3>' + post.title + '</h3><p>' + post.description + '</p>'))
    .addTo(map);

// toggle edit review form
const toggles = document.querySelectorAll('.toggle-edit-form');

toggles.forEach(toggle => {
    toggle.addEventListener('click', function() {
        this.textContent === 'Edit' ? this.textContent = 'Cancel' : this.textContent = 'Edit';
        this.nextElementSibling.classList.toggle('edit-review-form');
    });
});

const clearRatings = document.querySelectorAll('.clear-rating');
clearRatings.forEach(clearRating => {
    clearRating.addEventListener('click', function() {
        clearRating.nextElementSibling.click();
    });
});